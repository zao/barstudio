#pragma once

#include "Waveform.h"

#include <string>
#include <vector>
#include <stdint.h>

#include "json/json-forwards.h"

struct ChunkingParameters {
	ChunkingParameters(int elementCount, int bucketCount)
		: elementCount(elementCount)
		, bucketCount(bucketCount)
		, baseBucketSize(elementCount / bucketCount)
		, extraFraction(elementCount % bucketCount)
	{}

	unsigned int elementCount;
	unsigned int bucketCount;
	unsigned int baseBucketSize;
	unsigned int extraFraction;

	unsigned int CapacityNeeded() const {
		return extraFraction > 0 ? baseBucketSize+1 : baseBucketSize;
	}

	bool BucketHasExtraSample(unsigned int bucket) const {
		if (bucket == 0)
			return false;
		unsigned int next = bucket+1;
		bool hasExtra = (extraFraction*bucket)/bucketCount != (extraFraction*next)/bucketCount;
		return hasExtra;
	}

	uint64_t BucketSize(unsigned int bucket) const {
		if (BucketHasExtraSample(bucket)) {
			return baseBucketSize + 1;
		}
		return baseBucketSize;
	}
};

using Chunk = std::vector<std::vector<float>>;

void ResolveChunk(int chunkLength, int currentBucket, Chunk const& chunk, Waveform& target);

static int const cacheFormatVersion = 1;

bool DeserializeWaveform(Json::Value const& root, Waveform& wf);

bool SerializeWaveform(Waveform const& wf, Json::Value& root);

Waveform ScanWaveform(std::string track, bool tryCache = true);