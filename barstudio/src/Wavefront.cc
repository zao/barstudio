#include "Wavefront.h"

WavefrontFill WavefrontFill::MakeFlat(NVGcolor color) {
	WavefrontFill ret;
	ret.kind = Flat;
	auto& u = ret.flat;
	u.color = color;
	return ret;
}

WavefrontFill WavefrontFill::MakeEdgeToEdgeGradient(NVGcolor srcColor, NVGcolor dstColor) {
	WavefrontFill ret;
	ret.kind = EdgeToEdgeGradient;
	auto& u = ret.edgeToEdgeGradient;
	u.srcColor = srcColor;
	u.dstColor = dstColor;
	return ret;
}

WavefrontFill WavefrontFill::MakeSizedGradient(NVGcolor srcColor, NVGcolor dstColor, float srcY, float dstY) {
	WavefrontFill ret;
	ret.kind = SizedGradient;
	auto& u = ret.sizedGradient;
	u.srcColor = srcColor;
	u.dstColor = dstColor;
	u.srcY = srcY;
	u.dstY = dstY;
	return ret;
}

IWavefront::Ptr MakeEnergyWavefront() {
	auto ret = std::make_unique<MemberWavefront>(&Waveform::energies);
	return ret;
}

IWavefront::Ptr MakeMinimumWavefront() {
	auto ret = std::make_unique<MemberWavefront>(&Waveform::minimums);
	return ret;
}

IWavefront::Ptr MakeMaximumWavefront() {
	auto ret = std::make_unique<MemberWavefront>(&Waveform::maximums);
	return ret;
}

IWavefront::Ptr MakeConstantWavefront(float constant) {
	auto ret = std::make_unique<ConstantLineWavefront>(constant);
	return ret;
}
