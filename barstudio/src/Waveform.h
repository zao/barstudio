#pragma once

#include <vector>
#include <stddef.h>

using WavefrontData = std::vector<float>;

struct Waveform {
	std::vector<WavefrontData> minimums;
	std::vector<WavefrontData> maximums;
	std::vector<WavefrontData> energies;
	size_t width;
	size_t channels;
};

Waveform MakeSilentWaveform(int channels);