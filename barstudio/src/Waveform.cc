#include "Waveform.h"

Waveform MakeSilentWaveform(int channels) {
	Waveform ret{};
	ret.width = 8192;
	ret.channels = channels;
	for (size_t ch = 0; ch < ret.channels; ++ch) {
		ret.minimums.emplace_back(ret.width);
		ret.maximums.emplace_back(ret.width);
		ret.energies.emplace_back(ret.width);
	}
	return ret;
}