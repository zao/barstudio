#pragma once

#include <algorithm>
#include <functional>
#include <memory>
#include <vector>
#include "Waveform.h"

#include "nanovg.h"

struct WavefrontFill {
	enum {
		Flat,
		EdgeToEdgeGradient,
		SizedGradient,
	} kind;
	union {
		struct {
			NVGcolor color;
		} flat;
		struct {
			NVGcolor srcColor;
			NVGcolor dstColor;
		} edgeToEdgeGradient;
		struct {
			NVGcolor srcColor;
			NVGcolor dstColor;
			float srcY;
			float dstY;
		} sizedGradient;
	};

	static WavefrontFill MakeFlat(NVGcolor color);

	static WavefrontFill MakeEdgeToEdgeGradient(NVGcolor srcColor, NVGcolor dstColor);

	static WavefrontFill MakeSizedGradient(NVGcolor srcColor, NVGcolor dstColor, float srcY, float dstY);
};

struct IWavefront {
	virtual ~IWavefront() {}
	virtual WavefrontData Values(Waveform const& wf, int channel) const = 0;

	using Ptr = std::unique_ptr<IWavefront>;
};

struct MemberWavefront : IWavefront {
	using Projection = std::vector<WavefrontData> (Waveform::*);
	explicit MemberWavefront(Projection field)
		: field(field)
	{}

	virtual WavefrontData Values(Waveform const& wf, int channel) const {
		WavefrontData ret = (wf.*field)[channel];
		return ret;
	}

private:
	Projection field;
};

IWavefront::Ptr MakeEnergyWavefront();

IWavefront::Ptr MakeMinimumWavefront();

IWavefront::Ptr MakeMaximumWavefront();

struct ConstantLineWavefront : IWavefront {
	explicit ConstantLineWavefront(float constant)
		: constant(constant)
	{}

	virtual WavefrontData Values(Waveform const& wf, int channel) const {
		WavefrontData ret(wf.width, constant);
		return ret;
	}

private:
	size_t sampleCount;
	float constant;
};

IWavefront::Ptr MakeConstantWavefront(float constant);

struct TransformedWavefront : IWavefront {
	template <typename F>
	TransformedWavefront(IWavefront::Ptr&& source, F&& transform)
		: source(std::move(source))
		, transform(transform)
	{}

	virtual WavefrontData Values(Waveform const& wf, int channel) const {
		WavefrontData ret = source->Values(wf, channel);
		std::transform(ret.begin(), ret.end(), ret.begin(), transform);
		return ret;
	}

private:
	std::function<float (float)> transform;
	IWavefront::Ptr source;
};

template <typename F>
IWavefront::Ptr MakeTransformedWavefront(IWavefront::Ptr&& source, F&& transform) {
	auto ret = std::make_unique<TransformedWavefront>(std::move(source), transform);
	return ret;
}