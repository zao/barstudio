#include "Analysis.h"

#include <algorithm>
#include <fstream>
#include <string.h>

#include "json/json.h"
#include "stb_vorbis.h"

void ResolveChunk(int chunkLength, int currentBucket, Chunk const & chunk, Waveform & target) {
	auto channelCount = chunk.size();
	for (int ch = 0; ch < channelCount; ++ch) {
		auto& data = chunk[ch];
		float min = 0.0f;
		float max = 0.0f;
		float rms = 0.0f;
		if (!data.empty()) {
			auto end = data.begin() + chunkLength;
			auto IP = std::minmax_element(data.begin(), end);
			min = *IP.first;
			max = *IP.second;
			std::for_each(data.begin(), end, [&rms](float sample) {
				rms += sample * sample;
			});
			rms = sqrtf(rms / chunkLength);
		}
		target.minimums[ch][currentBucket] = min;
		target.maximums[ch][currentBucket] = max;
		target.energies[ch][currentBucket] = rms;
	}
}

bool DeserializeWaveform(Json::Value const & root, Waveform & wf) {
	if (root["version"] != cacheFormatVersion) {
		return false;
	}
	auto& channels = root["channels"];
	wf.channels = channels.size();
	if (wf.channels == 0 || wf.channels > 18) {
		return false;
	}
	for (auto channel : root["channels"]) {
		auto& srcMins = channel["minimums"];
		auto& srcMaxs = channel["maximums"];
		auto& srcEngs = channel["energies"];
		if (srcMins.size() != srcMaxs.size() || srcMins.size() != srcEngs.size()) {
			return false;
		}
		wf.width = srcMins.size();
		{
			std::vector<float> mins;
			for (auto& s : srcMins) {
				mins.push_back(s.asFloat());
			}
			wf.minimums.push_back(std::move(mins));
		}
		{
			std::vector<float> maxs;
			for (auto& s : channel["maximums"]) {
				maxs.push_back(s.asFloat());
			}
			wf.maximums.push_back(std::move(maxs));
		}
		{
			std::vector<float> engs;
			for (auto& s : channel["energies"]) {
				engs.push_back(s.asFloat());
			}
			wf.energies.push_back(std::move(engs));
		}
	}
	return true;
}

bool SerializeWaveform(Waveform const & wf, Json::Value & root) {
	root["version"] = cacheFormatVersion;
	auto& channels = root["channels"];
	for (int ch = 0; ch < wf.channels; ++ch) {
		Json::Value channel;
		auto& mins = channel["minimums"];
		for (auto& s : wf.minimums[ch]) {
			mins.append(s);
		}
		auto& maxs = channel["maximums"];
		for (auto& s : wf.maximums[ch]) {
			maxs.append(s);
		}
		auto& engs = channel["energies"];
		for (auto& s : wf.energies[ch]) {
			engs.append(s);
		}
		channels.append(std::move(channel));
	}
	return true;
}

Waveform ScanWaveform(std::string track, bool tryCache) {
	std::string cacheFilename = track + ".waveform";
	{
		std::ifstream is(cacheFilename.c_str());
		Json::Value root;
		Json::Reader r;
		Waveform wf;
		if (r.parse(is, root, false) && DeserializeWaveform(root, wf)) {
			return wf;
		}
	}
	int error{};
	auto* vrb = stb_vorbis_open_filename(track.c_str(), &error, nullptr);
	auto info = stb_vorbis_get_info(vrb);
	int channels = info.channels;
	Waveform ret = MakeSilentWaveform(channels);
	unsigned int frameCount = stb_vorbis_stream_length_in_samples(vrb);
	int N = 8192;
	if (frameCount < static_cast<unsigned int>(8 * N)) {
		return ret;
	}
	ChunkingParameters cps(frameCount, N);
	std::vector<std::vector<float>> chunk;
	unsigned int samplesInBucket = 0;
	unsigned int currentBucket = 0;
	for (int i = 0; i < channels; ++i) {
		std::vector<float> v;
		v.reserve(cps.CapacityNeeded());
		v.resize(cps.BucketSize(0));
		chunk.emplace_back(std::move(v));
	}
	float** frame;
	while (int newSamples = stb_vorbis_get_frame_float(vrb, &channels, &frame)) {
		int used = 0;
		int left = newSamples;
		while (currentBucket < cps.bucketCount) {
			auto bucketSize = static_cast<int>(cps.BucketSize(currentBucket));
			if (samplesInBucket == 0) {
				for (auto& ch : chunk) {
					ch.resize(bucketSize);
				}
			}
			int k = bucketSize - samplesInBucket;
			bool fillsBucket = k <= left;
			bool insufficient = k > left;
			if (insufficient) {
				k = left;
			}
			for (int ch = 0; ch < channels; ++ch) {
				float* src = frame[ch] + used;
				float* dst = chunk[ch].data() + samplesInBucket;
				memcpy(dst, src, k * sizeof(float));
			}
			samplesInBucket += k;
			used += k;
			left -= k;
			if (fillsBucket) {
				for (int ch = 0; ch < channels; ++ch) {
					ResolveChunk(bucketSize, currentBucket, chunk, ret);
				}
				++currentBucket;
				samplesInBucket = 0;
			}
			if (insufficient) {
				break;
			}
		}
	}
	stb_vorbis_close(vrb);
	{
		Json::Value root;
		if (SerializeWaveform(ret, root)) {
			Json::StyledStreamWriter ssw;
			std::ofstream os((track + ".waveform").c_str());
			ssw.write(os, root);
		}
	}
	return ret;
}
