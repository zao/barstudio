#include <algorithm>
#include <atomic>
#include <string>
#include <cassert>
#include <fstream>
#include <functional>
#include <iterator>
#include <list>
#include <memory>
#include <random>
#include <vector>

#include <Windows.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Analysis.h"
#include "Wavefront.h"

#include "json/json.h"
#include "nanovg.h"
#include "nanovg_gl.h"
#include "stb_image.h"
#include "stb_vorbis.h"

static int numWaveforms;
static int currentWaveformIndex;
static int desiredWaveformIndex;

void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE && mods == 0) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT) && mods == 0) {
		if (numWaveforms > 0) {
			desiredWaveformIndex = (desiredWaveformIndex + numWaveforms - 1) % numWaveforms;
		}
	}
	if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT)  && mods == 0) {
		if (numWaveforms > 0) {
			desiredWaveformIndex = (desiredWaveformIndex + 1) % numWaveforms;
		}
	}
}

std::vector<std::string> EnumerateAssets(std::string extension) {
	if (extension.empty()) {
		extension = ".*";
	}
	if (extension[0] != '.') extension = "." + extension;
	std::string prefix = "assets\\";
	std::string pattern = prefix + "*" + extension;
	WIN32_FIND_DATA findData{};
	HANDLE findHandle = FindFirstFileA(pattern.c_str(), &findData);
	std::vector<std::string> ret;
	if (findHandle == INVALID_HANDLE_VALUE) {
		return ret;
	}
	do {
		ret.push_back(prefix + findData.cFileName);
	} while (FindNextFileA(findHandle, &findData));
	FindClose(findHandle);
	return ret;
}

struct AABB2D {
	AABB2D(int x, int y, int w, int h)
		: x(x), y(y), w(w), h(h)
	{}

	int MapUi(float v) {
		return static_cast<int>(MapUf(v));
	}

	float MapUf(float v) {
		return x + w*v;
	}

	int MapVi(float v) {
		return static_cast<int>(MapVf(v));
	}

	float MapVf(float v) {
		v = -v * 0.5f + 0.5f;
		return y + h*v;
	}

	int x, y, w, h;
};

struct Layer {
	std::vector<IWavefront::Ptr> wavefronts;
	std::vector<WavefrontFill> fills;

	using Ptr = std::unique_ptr<Layer>;
};

struct Style {
	std::list<Layer::Ptr> layers;

	using Ptr = std::unique_ptr<Style>;
};

using TransformFunction = std::function<float(float)>;
using TransformFunctionPtr = std::unique_ptr<TransformFunction>;

TransformFunctionPtr CreateFunctionFromJson(Json::Value root) {
	return std::make_unique<TransformFunction>([](float v) { return v; });
}

IWavefront::Ptr CreateIWavefrontFromJson(Json::Value root) {
	auto kind = root["kind"];
	if (kind == "constant") {
		auto value = root["value"].asFloat();
		return MakeConstantWavefront(value);
	}
	else if (kind == "member") {
		auto field = root["field"].asString();
		if (field == "minimum") {
			return MakeMinimumWavefront();
		}
		else if (field == "maximum") {
			return MakeMaximumWavefront();
		}
		else if (field == "energies") {
			return MakeEnergyWavefront();
		}
		return {};
	}
	else if (kind == "transform") {
		auto waveform = CreateIWavefrontFromJson(root["waveform"]);
		if (!waveform) {
			return {};
		}
		auto function = CreateFunctionFromJson(root["function"]);
		if (!function) {
			return {};
		}
		return MakeTransformedWavefront(std::move(waveform), *function);
	}
	return {};
}

NVGcolor CreateColorFromJson(Json::Value root) {
	if (root.size() == 4) {
		float r = root[0].asFloat(), g = root[1].asFloat(), b = root[2].asFloat(), a = root[3].asFloat();
		return nvgRGBAf(r, g, b, a);
	}
	if (root.size() == 3) {
		float r = root[0].asFloat(), g = root[1].asFloat(), b = root[2].asFloat();
		return nvgRGBf(r, g, b);
	}
	return nvgRGBA(0, 0, 0, 0);
}

WavefrontFill CreateWavefrontFillFromJson(Json::Value root) {
	auto kind = root["kind"].asString();
	if (kind == "flat") {
		auto color = CreateColorFromJson(root["color"]);
		return WavefrontFill::MakeFlat(color);
	}
	else if (kind == "edge-to-edge-gradient") {
		auto srcColor = CreateColorFromJson(root["srcColor"]);
		auto dstColor = CreateColorFromJson(root["dstColor"]);
		return WavefrontFill::MakeEdgeToEdgeGradient(srcColor, dstColor);
	}
	else if (kind == "sized-gradient") {
		auto srcColor = CreateColorFromJson(root["srcColor"]);
		auto dstColor = CreateColorFromJson(root["dstColor"]);
		float srcY = root["srcY"].asFloat();
		float dstY = root["dstY"].asFloat();
		return WavefrontFill::MakeSizedGradient(srcColor, dstColor, srcY, dstY);
	}
	return WavefrontFill::MakeFlat(nvgRGBA(0, 0, 0, 0));
}

Style::Ptr CreateDemoStyleFromJson(Json::Value root) {
	auto versionField = root.get("version", Json::nullValue);
	if (!versionField.isInt()) return {};
	switch (versionField.asInt()) {
	case 1: {
		auto ret = std::make_unique<Style>();
		for (auto layerField : root["layers"]) {
			auto wavefrontsField = layerField["wavefronts"];
			auto fillsField = layerField["fills"];
			auto numWavefronts = wavefrontsField.size();
			auto numFills = fillsField.size();
			if (numWavefronts == 0 || numFills == 0 || numWavefronts != numFills + 1) {
				return {};
			}
			auto layer = std::make_unique<Layer>();
			for (auto wavefrontField : wavefrontsField) {
				layer->wavefronts.emplace_back(CreateIWavefrontFromJson(wavefrontField));
			}
			for (auto fillField : fillsField) {
				layer->fills.emplace_back(CreateWavefrontFillFromJson(fillField));
			}
			ret->layers.emplace_back(std::move(layer));
		}
		return ret;
	} break;
	default: {};
	}
	return {};
}

Style::Ptr CreateDemoStyle() {
	auto ret = std::make_unique<Style>();
	{
		auto layer = std::make_unique<Layer>();
		{
			IWavefront::Ptr v[] = {
				MakeMinimumWavefront(),
				MakeMaximumWavefront(),
			};
			layer->wavefronts.assign(std::make_move_iterator(begin(v)), std::make_move_iterator(end(v)));
		}

		auto signal = nvgRGBA(255, 255, 255, 255);
		layer->fills.emplace_back(WavefrontFill::MakeFlat(signal));
		ret->layers.emplace_back(std::move(layer));
	}
	{
		auto layer = std::make_unique<Layer>();
		{
			layer->wavefronts.emplace_back(MakeTransformedWavefront(MakeEnergyWavefront(), [](float v) { return -v; }));
			layer->wavefronts.emplace_back(MakeTransformedWavefront(MakeEnergyWavefront(), [](float v) { return +v; }));

			layer->fills.emplace_back(WavefrontFill::MakeFlat(nvgRGBA(0, 0, 255, 63)));
		}
		ret->layers.emplace_back(std::move(layer));
	}
	return ret;
}

template <typename F>
void EnumerateRegions(Layer const& layer, F&& f) {
	auto n = layer.fills.size();
	assert(n + 1 == layer.wavefronts.size());
	for (decltype(n) i = 0; i < n; ++i) {
		f(*layer.wavefronts[i].get(), *layer.wavefronts[i+1].get(), layer.fills[i]);
	}
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	glfwInit();
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_SAMPLES, 16);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);

	GLFWwindow* window = glfwCreateWindow(1280, 720, "Bar Studio", nullptr, nullptr);
	if (window == nullptr) {
		MessageBoxA(nullptr, "Could not create window", "Bar Studio failure", MB_OK);
		return 1;
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, &OnKey);
	{
		glewExperimental = GL_TRUE;
		auto res = glewInit();
		assert(res == GLEW_OK);
		while (glGetError()) {}
	}
	NVGcontext* vg = nvgCreateGL2(NVG_STENCIL_STROKES);

	std::vector<Waveform> waveforms;
	for (auto track : EnumerateAssets(".ogg")) {
		auto wf = ScanWaveform(track);
		waveforms.push_back(std::move(wf));
	}
	numWaveforms = static_cast<int>(waveforms.size());
	currentWaveformIndex = -1;

	auto style = CreateDemoStyle();

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		if (currentWaveformIndex != desiredWaveformIndex && desiredWaveformIndex < numWaveforms) {
			currentWaveformIndex = desiredWaveformIndex;
		}
		glClearColor(0.9f, 0.8f, 0.7f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		if (!waveforms.empty()) {
			auto& wf = waveforms.at(currentWaveformIndex);
			int w = 1, h = 1;
			glfwGetWindowSize(window, &w, &h);
			nvgBeginFrame(vg, w, h, 1.0f);

			AABB2D box(0, 0, w, h/5);
			nvgBeginPath(vg);
			nvgLineJoin(vg, NVG_ROUND);
			nvgStrokeWidth(vg, 0.5f);
			nvgMoveTo(vg, box.MapUf(0.0f), box.MapVf(0.0f));
			nvgLineTo(vg, box.MapUf(1.0f), box.MapVf(0.0f));
			nvgStrokeColor(vg, nvgRGBA(0, 0, 0, 255));
			nvgStroke(vg);
			for (auto& layer : style->layers) {
				EnumerateRegions(*layer, [&](IWavefront const& wavefrontA, IWavefront const& wavefrontB, WavefrontFill const& fill) {
					nvgBeginPath(vg);
					nvgLineCap(vg, NVG_BUTT);
					for (int i = 0; i < box.w; ++i) {
						float u = i / static_cast<float>(box.w - 1);
						int idx = static_cast<int>(u * (wf.width - 1));
						float v = wavefrontA.Values(wf, 0)[idx];
						float x = box.MapUf(u);
						float y = box.MapVf(v);
						(i == 0 ? nvgMoveTo : nvgLineTo)(vg, x, y);
					}
					for (int i = box.w - 1; i >= 0; --i) {
						float u = i / static_cast<float>(box.w - 1);
						int idx = static_cast<int>(u * (wf.width - 1));
						float v = wavefrontB.Values(wf, 0)[idx];
						float x = box.MapUf(u);
						float y = box.MapVf(v);
						nvgLineTo(vg, x, y);
					}
					if (fill.kind == WavefrontFill::Flat) {
						auto c = fill.flat.color;
						nvgFillColor(vg, c);
						nvgFill(vg);
					}
					else if (fill.kind == WavefrontFill::SizedGradient) {
						auto& sized = fill.sizedGradient;
						auto srcY = (1.0f - sized.srcY) * box.h;
						auto dstY = (1.0f - sized.dstY) * box.h;
						auto paint = nvgLinearGradient(vg, 0.0f, srcY, 0.0f, dstY, sized.srcColor, sized.dstColor);
						nvgFillPaint(vg, paint);
						nvgFill(vg);
					}
					else {
					}
				});
			}
			nvgEndFrame(vg);
		}
		glfwSwapBuffers(window);
	}
	glfwTerminate();

	return 0;
}