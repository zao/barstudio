add_executable(barstudio WIN32
	"src/Analysis.cc"
	"src/Analysis.h"
	"src/StudioMain.cc"
	"src/Waveform.cc"
	"src/Waveform.h"
	"src/Wavefront.cc"
	"src/Wavefront.h"
	"src/json/json-forwards.h"
	"src/json/json.h"
	"src/jsoncpp.cpp"
	"src/stb_image.h"
	"src/stb_vorbis.c"
)

if(WIN32)
	find_path(GLFW3_INCLUDE_DIR "GLFW/glfw3.h")
	find_library(GLFW3_LIBRARY "glfw3")
	set(GLFW3_LIBRARY ${GLFW3_LIBRARY} opengl32.lib)

	find_path(GLEW_INCLUDE_DIR "GL/glew.h")
	find_library(GLEW_LIBRARY "glew32s")
	set(GLEW_DEFINITIONS "-DGLEW_STATIC")
endif()

if (NOT MSVC)
	target_compile_options(barstudio
	PUBLIC
	"-std=c++14"
)
endif()

find_path(NANOVG_INCLUDE_DIR "nanovg.h")
find_library(NANOVG_LIBRARY "nanovg")
set(NANOVG_DEFINITIONS "-DNANOVG_GLEW=1" "-DNANOVG_GL2_IMPLEMENTATION=1")

target_compile_definitions(barstudio
	PRIVATE
	${GLEW_DEFINITIONS}
	${NANOVG_DEFINITIONS}
	"-D_CRT_SECURE_NO_WARNINGS=1"
	"-D_SCL_SECURE_NO_WARNINGS=1"
	"-D_SECURE_SCL=0"
	"-DSTB_IMAGE_STATIC=1"
	"-DSTB_IMAGE_IMPLEMENTATION=1"
)

target_include_directories(barstudio
	PRIVATE ${GLFW3_INCLUDE_DIR} ${GLEW_INCLUDE_DIR} ${NANOVG_INCLUDE_DIR}
)

target_link_libraries(barstudio
	PRIVATE ${GLFW3_LIBRARY} ${GLEW_LIBRARY} ${NANOVG_LIBRARY}
)
